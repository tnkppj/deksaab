def tran(number):
    r = ''
    if not (str(number).isdigit()):
        return 'Invalid'
    number = int(number)
    if number > 1000 or number == 0:
        return 'out'
    if number == 1000:
        return 'M'
    if number%10 < 4 :
        for i in range(number%10):
            r = 'I{}'.format(r)
    elif number%10 == 4 :
        r = 'IV{}'.format(r)
    elif number%10 < 9:
        r = '{}V'.format(r)
        for i in range(number%10-5):
            r = '{}I'.format(r)
    elif number%10 == 9:
        r = '{}IX'.format(r)
#----------------------------------------------------------
    # if int((number%100)/10) < 4:
    #     for i in range(int((number%100)/10)):
    #         r = 'X{}'.format(r)
    if int((number%100)/10) < 4:
        r = '{}{}'.format(('X'*int((number%100)/10)),r)
    elif int((number%100)/10) == 4:
        r = 'XL{}'.format(r)
    elif int((number%100)/10) < 9:
        for i in range(int((number%100)/10)-5):
            r = 'X{}'.format(r)
        r = 'L{}'.format(r)
    elif int((number%100)/10) == 9:
        r = 'XC{}'.format(r)
#----------------------------------------------------------
    if int((number%1000)/100) < 4:
        for i in range(int((number%1000)/100)):
            r = 'C{}'.format(r)
    elif int((number%1000)/100) == 4:
        r = 'CD{}'.format(r)
    elif int((number%1000)/100) < 9:
        for i in range(int((number%1000)/100)-5):
            r = 'C{}'.format(r)
        r = 'D{}'.format(r)
    elif int((number%1000)/100) == 9:
        r = 'CM{}'.format(r)
    return r